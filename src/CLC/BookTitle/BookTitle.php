<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Utility\Utility;

use App\Model\Database as DB;
use PDO;
use PDOException;

class BookTitle extends DB{
    private $id;
    private $bookName;
    private $AuthorName;

    public function setData($postdata){


    	if (array_key_exists('id',$postdata)) {$this->id=$postdata['id'];}
    	if (array_key_exists('bookName',$postdata)) {$this->bookName=$postdata['bookName'];}
    	if (array_key_exists('AuthorName',$postdata)) {$this->AuthorName=$postdata['AuthorName'];}
        //var_dump($postdata);

	}
  public function store(){
        $arrData = array($this->bookName,$this->AuthorName);
        

        $sql="INSERT into book(bookName,AuthorName)  VALUES (?,?)";
        var_dump($this->DBH); 
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);


        if($result)
            Message::message("Success!");
        else
             Message::message("Failed!");
         Utility::redirect('index.php');

    }

    public function index(){
        $sql = "select * from book where soft_delete='NO'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();

}


public function view(){
        $sql = "select * from book where id =".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode (PDO::FETCH_OBJ);
        return $STH->fetch();
   
    }


    public function update(){
        $arrData = array($this->bookName,$this->AuthorName);
        //var_dump($arrData);

        $sql="UPDATE  book SET bookName=?,AuthorName=? WHERE id=".$this->id;
        var_dump($this->DBH);
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);




   if ($result)
    Message::message("Success!");
        else
            Message::message("Failed");
        Utility::redirect('index.php');


}

      public function delete(){
        

        
       
       $sql="delete from book WHERE id=".$this->id;
       $result=$this->DBH->exec($sql);


        if($result)
            Message::message("Success!");
        else
             Message::message("Failed!");
         Utility::redirect('index.php');

    }
}



?>